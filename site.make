core = 8.x
api = 2

; MUR base module
projects[uw_mur_base][type] = "module"
projects[uw_mur_base][download][type] = "git"
projects[uw_mur_base][download][url] = "https://git.uwaterloo.ca/mur-wcms-3/uw_mur_base.git"
projects[uw_mur_base][download][tag] = "1.0.8"
projects[uw_mur_base][subdir] = ""

; Admissions Requirements
projects[uw_admission_requirements][type] = "module"
projects[uw_admission_requirements][download][type] = "git"
projects[uw_admission_requirements][download][url] = "https://git.uwaterloo.ca/mur-wcms-3/uw_admission_requirements.git"
projects[uw_admission_requirements][download][tag] = "1.0.17"
projects[uw_admission_requirements][subdir] = ""

; Budget Calculator
projects[uw_budget_calculator][type] = "module"
projects[uw_budget_calculator][download][type] = "git"
projects[uw_budget_calculator][download][url] = "https://git.uwaterloo.ca/mur-wcms-3/uw_budget_calculator.git"
projects[uw_budget_calculator][download][tag] = "1.0.8"
projects[uw_budget_calculator][subdir] = ""

; Brochure Request
projects[uw_brochure_request][type] = "module"
projects[uw_brochure_request][download][type] = "git"
projects[uw_brochure_request][download][url] = "https://git.uwaterloo.ca/mur-wcms-3/uw_brochure_request.git"
projects[uw_brochure_request][download][tag] = "1.0.10"
projects[uw_brochure_request][subdir] = ""
